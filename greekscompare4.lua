--test comment
Class_Fut  	= "SPBFUT"     -- ����� ��������
Class_Opt 	= "SPBOPT"
Sec_Fut    	= "SiZ1"       -- ��� ��������
Year       	= 365          -- ����� ���� � ����
Step_Fut   	= 250          -- ������ ������� �� Step_Fut
isRun		= true
RiskFree	= 0/100 --����������� ������ %, ����������� �������
OptsChanged	= false
count		= 0
maincount	= 0
checkedcount= 0
temptime2	= os.time();
bidnotniltheta	= 0
offernotniltheta= 0

function OnInit() -- ������� ���������� ���������� QUIK ����� ������� ������� main()
end

function m(arg)
	if arg==nil then
		return "nil"
	else
		return arg
	end
end

function main() -- �������, ����������� �������� ����� ���������� � �������
    local info 			= debug.getinfo(1,'S');
    local fname 		= string.sub(info.source, 2, #info.source - 4)..".log"
    local f 			= io.open(fname,"a+")
	local ErrMessTime	= 0
	f:write("NewRun;"..os.date("%d/%m/%Y %X")..";\n")
	f:flush();
    -- ���������� � ���� ������� ���������
    OptCodeList = {}
	Opts		= {}
	opair		= {}
	opair2		= {}
	MessStr		= ""

   -- ������� �������
   t_id = AllocTable() -- �������� ��������� id ��� ��������
   -- ��������� �������
   AddColumn(t_id, 0, "������", true, QTABLE_STRING_TYPE, 15)
   AddColumn(t_id, 1, "�������������", true, QTABLE_INT_TYPE, 7)
   AddColumn(t_id, 2, "�����", true, QTABLE_INT_TYPE, 4)
   AddColumn(t_id, 3, "����", true, QTABLE_INT_TYPE, 7)
   AddColumn(t_id, 4, "������", true, QTABLE_INT_TYPE, 9)
   AddColumn(t_id, 5, "Profit", true, QTABLE_INT_TYPE, 7)
   AddColumn(t_id, 6, "P/L", true, QTABLE_INT_TYPE, 10)
   t = CreateWindow(t_id) -- ������� �������
   local info = debug.getinfo(1,'S');
   fname = string.sub(info.source, 2, #info.source - 4)
   SetWindowCaption(t_id, fname) -- ������������� ���������
   SetWindowPos(t_id, 000, 451, 390, 132) -- ������ ��������� � ������� ���� �������
   for i = 1, 5, 1 do InsertRow(t_id, i) end -- ��������� ������
   -- ��������� �������� � ������
   --SetCell(t_id, 1, 0, Short_Call); SetCell(t_id, 2, 0, Option3); SetCell(t_id, 3, 0, First_Option); SetCell(t_id, 4, 0, Second_Option)
--local x = 1
--local y = find_zero(my_func, 1.0, 1000.0,0.00001,19)
--message(""..y)       -->  28.643931367544
--isRun=false
	--local V=FindVolatility("Si73000BL1",1200,72764)
	--local co=Options3("Si73000BL1",V,72764)
	--message(""..co.days_to_mat_date.." "..co.TheorPrice.." "..V)
	while isRun do
		if tonumber(getParamEx(Class_Fut, Sec_Fut, "status").param_value) == 1 then -- �������� ��������� ��� ���
			maincount=maincount+1
			--message(""..maincount)
			Opts2	= {}
			--sleep(1000)
			--message("");
			local idx=0;
		    for OptCode in getClassSecurities("SPBOPT"):gmatch("[^,]+") do
				if string.sub(OptCode, 1, 2) == "Si" then -- "Si79250BJ1" string.sub(OptCode, 1, 10) == "Si73250BX1" or string.sub(OptCode, 1, 10) == "Si72750BV1" then
				--message(""..OptCode.." "..os.time());
		        	--OptCodeList[#OptCodeList + 1] = OptCode
					co=Options3(OptCode)
					--message(""..OptCode..";"..co.Gamma..";"..co.Theta);-- sleep(1000)
					osdate=os.date("%d/%m/%Y %X")
		
					if co.Theta==nil then
						message("Theta==nil "..co.OptCode);
						if os.time()-ErrMessTime>1 then
							sleep(100);
						else
							sleep(10000);
						end;
						ErrMessTime=os.time();
					elseif co.days_to_mat_date>1 then
						local settleprice = tonumber(getParamEx(Class_Fut, co.futers, "settleprice").param_value)
						local cbid, coffer
						numoffers=tonumber(getParamEx(Class_Opt, OptCode, "numoffers").param_value)
						offerprice=tonumber(getParamEx(Class_Opt, OptCode, "offer").param_value)
						if numoffers>0 then
							offervolatility=FindVolatility(OptCode, offerprice, settleprice);
							if offervolatility~=nil then
								--message(""..OptCode.." "..offerprice)--.." "..co.STheta);
								coffer = Options3(OptCode, offervolatility, settleprice)
								if coffer.TheorPrice~=offerprice then
									if not IsSubscribed_Level_II_Quotes(Class_Opt, OptCode) then
								    	Subscribe_Level_II_Quotes(Class_Opt, OptCode)
								   	end
								   	local qu = getQuoteLevel2 (Class_Opt, OptCode)
								   	if tonumber(qu.offer_count)>0 then
									   	if tonumber(qu.offer_count)~=numoffers then
									   		message(""..OptCode.." qu.offer_count "..tonumber(qu.offer_count).." numoffers "..numoffers)
									   	end
										message("coffer.TheorPrice<>offerprice "..OptCode.." "..coffer.TheorPrice.." "..offerprice);
										sleep(3000)
									else
										coffer = {OfferTheta=nil,OfferGamma=nil}
									end
								end
								--message(""..OptCode.." coffer "..coffer.Theta)--.." "..co.STheta);
							else
								coffer = {OfferTheta=nil,OfferGamma=nil}
							end
						else
							offervolatility=""
							coffer = {OfferTheta=nil,OfferGamma=nil}
						end
						if offerprice==nil then
							offerprice=""
						end
						
						numbids=tonumber(getParamEx(Class_Opt, OptCode, "numbids").param_value)
						bidprice=tonumber(getParamEx(Class_Opt, OptCode, "bid").param_value)
						if numbids>0 then
							if bidprice==nil then
								message(""..OptCode.." bidprice==nil")
							end
							bidvolatility=FindVolatility(OptCode, bidprice, settleprice);
							if bidvolatility~=nil then
								--message(""..OptCode.." bidprice "..m(bidprice))--.." "..co.STheta);
								cbid = Options3(OptCode, bidvolatility, settleprice)
								if cbid.TheorPrice~=bidprice then
									message("cbid.TheorPrice<>bidprice "..OptCode.." "..cbid.TheorPrice.." "..bidprice);
									sleep(3000)
								end
								--message(""..OptCode.." cbid "..cbid.Theta)--.." "..co.STheta);
							else
								cbid = {bidTheta=nil,bidGamma=nil}
							end
						else
							bidvolatility=""
							cbid = {bidTheta=nil,bidGamma=nil}
						end
						if bidprice==nil then
							bidprice=""
						end
						
						--if not IsSubscribed_Level_II_Quotes(Class_Opt, OptCode) then
					    --	Subscribe_Level_II_Quotes(Class_Opt, OptCode)
					   	--end
					   	--sleep(1000)
					   	--local qu = getQuoteLevel2 (Class_Opt, OptCode)
					   	--if tonumber(qu.offer_count)~=tonumber(getParamEx(Class_Opt, OptCode, "numoffers").param_value) then
					   	--	message(""..OptCode.." qu.offer_count "..tonumber(qu.offer_count).." getParamEx(Class_Opt, OptCode, numoffers) "..
					   	--		getParamEx(Class_Opt, OptCode, "numoffers").param_value)
					   	--end
						--if tonumber(qu.offer_count)>0 then
						--	offerqty=qu.offer[1].quantity
						--	offerprice=tonumber(qu.offer[1].price)
						--   	if tonumber(qu.offer[1].price)~=tonumber(getParamEx(Class_Opt, OptCode, "offer").param_value) then
						--   		message(""..OptCode.." qu.offer[1].price "..tonumber(qu.offer[1].price).." getParamEx(Class_Opt, OptCode, offer) "..
						--   			getParamEx(Class_Opt, OptCode, "offer").param_value)
						--   	end
						--	offervolatility=FindVolatility(OptCode, offerprice, settleprice);
						--	if offervolatility~=nil then
						--		message(""..OptCode.." "..offerprice)--.." "..co.STheta);
						--		coffer = Options3(OptCode, offervolatility, settleprice)
						--		if coffer.TheorPrice~=offerprice then
						--			message("coffer.TheorPrice<>offerprice "..OptCode.." "..coffer.TheorPrice.." "..offerprice);
						--			sleep(3000)
						--		end
						--		message(""..OptCode.." coffer "..coffer.Theta)--.." "..co.STheta);
						--	else
						--		coffer = {OfferTheta=nil,OfferGamma=nil}
						--	end
						--else
						--	offerqty=""
						--	offerprice=""
						--	offervolatility=""
						--	coffer = {OfferTheta=nil,OfferGamma=nil}
						--end
					   	--if tonumber(qu.bid_count)~=tonumber(getParamEx(Class_Opt, OptCode, "numbids").param_value) then
					   	--	message(""..OptCode.." qu.bid_count "..tonumber(qu.bid_count).." getParamEx(Class_Opt, OptCode, numbids) "..
					   	--		getParamEx(Class_Opt, OptCode, "numbids").param_value)
					   	--end
						--if tonumber(qu.bid_count)>0 then
						--	if pcall(function () bidqty=qu.bid[tonumber(qu.bid_count)].quantity end) then
						--		bidqty=qu.bid[tonumber(qu.bid_count)].quantity
						--	   	if tonumber(qu.bid[tonumber(qu.bid_count)].price)~=tonumber(getParamEx(Class_Opt, OptCode, "bid").param_value) then
						--	   		message(""..OptCode.." qu.bid[tonumber(qu.bid_count)].price "..tonumber(qu.bid[tonumber(qu.bid_count)].price)..
						--	   			" getParamEx(Class_Opt, OptCode, bid) "..
						--	   			getParamEx(Class_Opt, OptCode, "bid").param_value)
						--	   	end
						--		bidprice=tonumber(qu.bid[tonumber(qu.bid_count)].price)
						--		bidvolatility=FindVolatility(OptCode, bidprice, settleprice);
						--		if bidvolatility~=nil then
						--			cbid = Options3(OptCode, bidvolatility, settleprice)
						--			if cbid.TheorPrice~=bidprice then
						--				message("cbid.TheorPrice<>bidprice "..OptCode.." "..cbid.TheorPrice.." "..bidprice.." "..bidvolatility);
						--				sleep(3000)
						--			end
						--			message(""..OptCode.." cbid "..cbid.Theta)--.." "..co.STheta);
						--		else
						--			cbid = {bidTheta=nil,bidGamma=nil}
						--		end
						--	else
						--		bidqty=""
						--		bidprice=""
						--		bidvolatility=""
						--		cbid = {BidTheta=nil,BidGamma=nil}
						--	end
						--else
						--	bidqty=""
						--	bidprice=""
						--	bidvolatility=""
						--	cbid = {BidTheta=nil,BidGamma=nil}
						--end
						if pcall(function () return Opts[OptCode].Theta end) then
							if Opts[OptCode].OfferTheta~=coffer.Theta then
								--message(""..OptCode.." OfferTheta "..m(Opts[OptCode].OfferTheta).." "..m(coffer.Theta))
								Opts[OptCode].OfferTheta=coffer.Theta
								OptsChanged=true
							end
							if Opts[OptCode].OfferGamma~=coffer.Gamma then
								--message(""..OptCode.." OfferGamma "..m(Opts[OptCode].OfferGamma).." "..m(coffer.Gamma))
								Opts[OptCode].OfferGamma=coffer.Gamma
								OptsChanged=true
							end
							if Opts[OptCode].OfferPrice~=coffer.TheorPrice then
								--message(""..OptCode.." OfferPrice "..m(Opts[OptCode].OfferPrice).." "..m(coffer.TheorPrice))
								Opts[OptCode].OfferPrice=coffer.TheorPrice
								OptsChanged=true
							end
							if Opts[OptCode].BidTheta~=cbid.Theta then
								--message(""..OptCode.." BidTheta "..m(Opts[OptCode].BidTheta).." "..m(cbid.Theta))
								Opts[OptCode].BidTheta=cbid.Theta
								OptsChanged=true
							end
							if Opts[OptCode].BidGamma~=cbid.Gamma then
								--message(""..OptCode.." BidGamma "..m(Opts[OptCode].BidGamma).." "..m(cbid.Gamma))
								Opts[OptCode].BidGamma=cbid.Gamma
								OptsChanged=true
							end
							if Opts[OptCode].BidPrice~=cbid.TheorPrice then
								--message(""..OptCode.." BidPrice "..m(Opts[OptCode].BidPrice).." "..m(cbid.TheorPrice))
								Opts[OptCode].BidPrice=cbid.TheorPrice
								OptsChanged=true
							end
							if Opts[OptCode].Optiontype~=co.Optiontype then
								--message(""..OptCode.." Optiontype  "..m(Opts[OptCode].Optiontype).." "..m(co.Optiontype))
								Opts[OptCode].Optiontype=co.Optiontype
								OptsChanged=true
							end
							if Opts[OptCode].days_to_mat_date~=co.days_to_mat_date then
								--message(""..OptCode.." days_to_mat_date "..m(Opts[OptCode].days_to_mat_date).." "..m(co.days_to_mat_date))
								Opts[OptCode].days_to_mat_date=co.days_to_mat_date
								OptsChanged=true
							end
						else
						--message(""..OptCode);
						--message(""..OptCode.." "..coffer.Theta.." "..cbid.Theta)--.." "..co.STheta);
							Opts[OptCode]={idx=idx,
							OfferTheta=coffer.Theta,
							OfferGamma=coffer.Gamma,
							OfferPrice=coffer.TheorPrice,
							BidTheta=cbid.Theta,BidGamma=cbid.Gamma,BidPrice=cbid.TheorPrice,
								Optiontype=co.Optiontype,days_to_mat_date=co.days_to_mat_date,settleprice=co.settleprice}
							if cbid.Theta~=nil then
								bidnotniltheta=bidnotniltheta+1
							end
							if coffer.Theta~=nil then
								offernotniltheta=offernotniltheta+1
							end
							OptsChanged=true
							--f:write(OptCode..";"..co.Theta..";"..co.Gamma..";"..co.Optiontype..";"..co.days_to_mat_date..";\n")
							--f:flush()
						end
						--if IsSubscribed_Level_II_Quotes(Class_Opt, OptCode) then
					   -- 	Unsubscribe_Level_II_Quotes(Class_Opt, OptCode)
					   	--end
					end
				end
		    end
			--table.sort(Opts)
			--table.sort(Opts, function(a, b)	return (a['idx'] or 0)<(b['idx'] or 0) end)
			idx=0;for i,co in pairs(Opts) do
				idx=idx+1;
				co.idx=idx;
			end

  			local temptime=os.time()
  			local temptime3
			SetCell(t_id, 2, 0, tostring(idx));
			SetCell(t_id, 2, 1, tostring(OptsChanged));
			checkedcount=0
			if OptsChanged then
				for i,co in pairs(Opts) do
					--message(""..co.idx.."/"..idx);
					if os.time()-temptime>60 then
						message("������ ����� ������")
						sleep(60000)
						temptime=os.time()
					end
					temptime3=os.date("%X")
					if temptime2~=temptime3 then
						SetCell(t_id, 1, 0, co.idx.."/"..idx);
						SetCell(t_id, 5, 0, temptime3);
						--message(""..co.idx.."/"..idx) --.." "..coo.idx.."/"..idx)
						temptime2=temptime3
					end
					for j,coo in pairs(Opts) do
						if co.BidTheta~=nil and coo.OfferTheta~=nil then
							checkedcount=checkedcount+1
						--message(""..co.idx.."/"..idx.." "..coo.idx.."/"..idx);
							--k=co.Gamma/coo.Gamma;
							--ct=round(coo.Theta-co.Theta/k,2)
							k=co.BidGamma/coo.OfferGamma;
							ct=round(coo.OfferTheta-co.BidTheta/k,2)
							if ct>2 and co.BidGamma>coo.OfferGamma then
								paircode=""..i..";"..j;
								if pcall(function () return opair[paircode].commontheta end) then
									if opair[paircode].commontheta~=ct then
										commontheta=""..opair[paircode].commontheta..";"..ct
										opair[paircode].commontheta=ct
									else
										commontheta=""
									end
									opair[paircode].found=true;
								else
									commontheta=ct
									opair[paircode]={paircode=paircode, commontheta=ct,start=osdate,found=true,new=true}
								end
								
								MessStr=""..commontheta..";"
								if MessStr~=";" then
									--f:write(paircode..";"..MessStr..osdate..";;\n")
									--f:write(paircode..";"..MessStr..osdate..";;;"..direction..";\n")
									--message(""..i..";"..co.Gamma..";"..co.Theta..";"..co.Optiontype..";"..co.days_to_mat_date..";"..
									--			j..";"..coo.Gamma..";"..coo.Theta..";"..coo.Optiontype..";"..coo.days_to_mat_date..";"..ct)
									--f:write(""..i..";"..j..";"..maincount.."\n");
									if opair[paircode].new then f:write(""..i..";"..j..";"..co.Optiontype..";"..coo.Optiontype..";"..
									co.days_to_mat_date..";"..coo.days_to_mat_date..";"..
									co.BidGamma..";"..coo.OfferGamma..";"..co.BidTheta..";"..coo.OfferTheta..";"..
									co.BidPrice..";"..coo.OfferPrice..";"..co.settleprice..";"..coo.settleprice..";"..ct.."\n")
									end
									f:flush()
									--sleep(100);
								end
							end
						end
					end
				end
				for i,p in pairs(opair) do
					if not p.found then
						f:write(i..";"..p.commontheta..";"..p.start..";"..osdate..";\n")
						f:flush()
						opair[i]=nil
					end
				end
				local pcount=0;
				for i,p in pairs(opair) do
					p.found=false;pcount=pcount+1;
					p.new=false
				end
				--f:write("pcount "..pcount..";\n")
				f:flush()
			end -- if OptsChanged then
			temptime3=os.date("%X")
			if temptime2~=temptime3 then
				SetCell(t_id, 5, 0, temptime3);
				temptime2=temptime3
			end
			SetCell(t_id, 4, 0, tostring(checkedcount));
			SetCell(t_id, 4, 1, tostring(bidnotniltheta));
			SetCell(t_id, 4, 2, tostring(offernotniltheta));
			OptsChanged=false
			if maincount>20 then
				isRun=false
			end
		else
			sleep(100);
		end;
	end
	  
    f:flush() -- ��������� ��������� � �����
    f:close() -- ��������� ����
end

function OnStop() -- ������� ���������� ���������� QUIK ��� ��������� ������� �� ������� ����������
	isRun=false
end

function Options(sec) -- ������� ������� ��������� �������
   local P = tonumber(getParamEx(Class_Fut, Sec_Fut, "settleprice").param_value) -- ������� ���� ��������
   local S = tonumber(string.sub(sec, 3, 7)) -- ������ �������
   local V = tonumber(getParamEx(Class_Opt, sec, "volatility").param_value) / 100 -- ������������� ������� � �����
   local D = tonumber(getParamEx(Class_Opt, sec, "days_to_mat_date").param_value) / Year -- ����� ���� �� ��������� � ����� ����
   local d1 = (math.log(P / S) + V * V * D / 2) / (V * math.sqrt(D))
   local d2 = d1 - V * math.sqrt(D)
   local Exchang = tonumber(getParamEx(Class_Opt, sec, "theorprice").param_value) -- ������������� ���� �����
   local Theta = (-P * V * math.exp(-D) * pN(d1)) / (2 * math.sqrt(D))
   local Theta_C = round((Theta - (S * math.exp(-D) * N(d2)) + P * math.exp(-D) * N(d1)) / Year, 0) -- ����� Call
   local Theta_P = round((Theta + (S * math.exp(-D) * N(-d2)) - P * math.exp(-D) * N(-d1)) / Year, 0) -- ����� Put
   local Delta_C = round(math.exp(-D) * N(d1), 2) -- ������ Call
   local Profit_C = round(Step_Fut * Delta_C, 0) -- ������ ��� Call
   local Delta_P = -round(-math.exp(-D) * N(-d1), 2) -- ������ Put
   local Profit_P = round(Step_Fut * Delta_P, 0) -- ������ ��� Put
	--message(""..V.." "..d1.." "..P.." "..S.." "..D)
   return {["Exchang"] = Exchang,
           ["Theta_C"] = Theta_C,
           ["Theta_P"] = Theta_P,
           ["Profit_C"] = Profit_C,
           ["Profit_P"] = Profit_P,
		   ["SettlePrice"] = P,
		   ["OptCode"] = sec,
		   ["bid"] = tonumber(getParamEx(Class_Opt, sec, "bid").param_value),
		   ["offer"] = tonumber(getParamEx(Class_Opt, sec, "offer").param_value),
		   ["days_to_mat_date"] = D*Year,
		   ["numbids"] = getParamEx(Class_Opt, sec, "numbids").param_value 
		   }
end

function strike(sec ) -- ������� ������� ��������� �������
	local st=0
	local fn=0
	local a=0
	local i=0
	for i = 1, #sec, 1 do
		a = string.byte(sec,i)
		if a>=48 and a<=57 then -- 0..9
			if st==0 then
				st=i
			end
		elseif st>0 then
			fn=i-1
			break
		end
	end
	return tonumber(string.sub(sec,st, fn))
end

function Options2(sec ,Operation) -- ������� ������� ��������� �������
   local localfut = getParamEx(Class_Opt, sec, "optionbase").param_image
   --local strike = getParamEx(Class_Opt, sec, "strike").param_value
   if getParamEx(Class_Opt, sec, "optiontype").param_image~="Put" then
   	iscall = true
   else
   	iscall = false
   end

   local S = strike(sec) -- tonumber(string.sub(sec, 3, 7)) -- ������ �������
   local V = tonumber(getParamEx(Class_Opt, sec, "volatility").param_value) / 100 -- ������������� ������� � �����
   local D = tonumber(getParamEx(Class_Opt, sec, "days_to_mat_date").param_value) / Year -- ����� ���� �� ��������� � ����� ����

   if IsSubscribed_Level_II_Quotes(Class_Fut, localfut)==false then
      Subscribe_Level_II_Quotes(Class_Fut, localfut)
   end
   local qu = getQuoteLevel2 (Class_Fut, localfut)
	if tonumber(qu.offer_count)>0 then
		--if (Operation=="�������" and iscall)or(Operation=="�������" and not iscall) then
			P=tonumber(qu.offer[1].price)
		--else
		--	P=tonumber(qu.bid[tonumber(qu.bid_count)].price)
		--end
		--message(""..P.." "..qu.bid[tonumber(qu.bid_count)].price);sleep(1000);
		futofferprice=P;
		futofferqty=qu.offer[1].quantity
   		local d1 = (math.log(P / S) + V * V * D / 2) / (V * math.sqrt(D)) -- ����� �� ����������� ����������� ���������� ������!!!
--	message(""..V.." "..d1.." "..P.." "..S.." "..D)
   		local d2 = d1 - V * math.sqrt(D)
		local ExchangCall, ExchangPut = Black_Scholes(P, S, V, D)
		--if (iscall and foffer) or ((not iscall) and (not foffer))then
		if iscall then
			-- ������ call ������� ������ ������� ��� ��������� ��������, ������ �������� �� ���� �������
			ExchangSell = ExchangCall
   			STheta, STheta_C, STheta_P, SDelta_C, SProfit_C, SDelta_P, SProfit_P = GreekCalc2(d1, d2, P, V, D, S)
   			message(sec.." "..STheta.." "..STheta_C.." "..STheta_P)
		else
			ExchangBay = ExchangPut
			BTheta, BTheta_C, BTheta_P, BDelta_C, BProfit_C, BDelta_P, BProfit_P = GreekCalc2(d1, d2, P, V, D, S)
   			message(sec.." "..BTheta.." "..BTheta_C.." "..BTheta_P)
		end
		--qu.offer[1].quantity
	else
		if datemesquotesoffer~=os.date() then
			message("�� �����, � ������� �������� - ����� (offer)"..localfut)
			datemesquotesoffer=os.date()
		end
		ExchangSell=100000
		ExchangBay=0
		ExchangBay=GeneriruemOshibku
	end
	--message("ExchangBay1"..ExchangBay)
	if tonumber(qu.bid_count)>0 then
		-- !!! ����� ��� �������, ��� �� ���� ������� �����-�� �������� ��������... if (Operation=="�������" and iscall)or(Operation=="�������" and not iscall) then
		P=tonumber(qu.bid[tonumber(qu.bid_count)].price)
		futbidprice=P
		futbidqty=qu.bid[tonumber(qu.bid_count)].quantity
   		local d1 = (math.log(P / S) + V * V * D / 2) / (V * math.sqrt(D))
   		local d2 = d1 - V * math.sqrt(D)
		local ExchangCall, ExchangPut = Black_Scholes(P, S, V, D)
		if iscall then
			-- ������ call ������� ������ ������� ��� ��������� �������, ������ ������� �� ���� �������
			ExchangBay = ExchangCall
			BTheta, BTheta_C, BTheta_P, BDelta_C, BProfit_C, BDelta_P, BProfit_P = GreekCalc2(d1, d2, P, V, D, S)
   			message(sec.." "..BTheta.." "..BTheta_C.." "..BTheta_P)
		else
			ExchangSell = ExchangPut
			STheta, STheta_C, STheta_P, SDelta_C, SProfit_C, SDelta_P, SProfit_P = GreekCalc2(d1, d2, P, V, D, S)
   			message(sec.." "..STheta.." "..STheta_C.." "..STheta_P)
		end
		--qu.bid[1].quantity
	else
		if datemesquotesbid~=os.date() then
			message("�� �����, � ������� �������� - ����� (bid)"..localfut)
			datemesquotesbid=os.date()
		end
		ExchangSell=100000
		ExchangBay=0
		ExchangBay=GeneriruemOshibku
	end
	--message("ExchangBay2"..ExchangBay)
   local P = tonumber(getParamEx(Class_Fut, localfut, "settleprice").param_value) -- ������� ���� ��������
   local d1 = (math.log(P / S) + V * V * D / 2) / (V * math.sqrt(D))
   local d2 = d1 - V * math.sqrt(D)
   local Exchang = tonumber(getParamEx(Class_Opt, sec, "theorprice").param_value) -- ������������� ���� �����
   --local Exchang2, Exchang3 = Black_Scholes(P, S, V, D)
	
   --if iscall then
   --	Exchang3=Exchang2
   --end
   local Theta = (-P * V * pN(d1)) / (2 * math.sqrt(D)) -- (-P * V * math.exp(-D) * pN(d1)) / (2 * math.sqrt(D))
   local Theta_C = round((Theta - (S * math.exp(-D) * N(d2)) + P * math.exp(-D) * N(d1)) / Year, 0) -- ����� Call
   local Theta_P = round((Theta + (S * math.exp(-D) * N(-d2)) - P * math.exp(-D) * N(-d1)) / Year, 0) -- ����� Put
   local Delta_C = round(math.exp(-D) * N(d1), 2) -- ������ Call
   local Profit_C = round(Step_Fut * Delta_C, 0) -- ������ ��� Call
   local Delta_P = -round(-math.exp(-D) * N(-d1), 2) -- ������ Put
   local Profit_P = round(Step_Fut * Delta_P, 0) -- ������ ��� Put
	message(Theta.." "..Theta_C.." "..Theta_P)
   return {["Exchang"] = Exchang,
   		   ["ExchangBay"]= ExchangBay,
   		   ["ExchangSell"]= ExchangSell,
           ["BDelta_C"] = BDelta_C,
           ["SDelta_C"] = SDelta_C,
           ["BDelta_P"] = BDelta_P,
           ["SDelta_P"] = SDelta_P,
		   ["SettlePrice"] = P,
		   ["futers"] = localfut,
		   ["iscall"] = iscall,
		   ["OptCode"] = sec,
		   ["bid"] = tonumber(getParamEx(Class_Opt, sec, "bid").param_value),
		   ["futbidprice"] = futbidprice,
		   ["futbidqty"] = futbidqty,
		   ["offer"] = tonumber(getParamEx(Class_Opt, sec, "offer").param_value),
		   ["futofferprice"] = futofferprice,
		   ["days_to_mat_date"] = D*Year,
		   ["volatility"] = V,
		   ["bidqty"] = getParamEx(Class_Opt, sec, "biddepth").param_value,
		   ["Theta_C"] = getParamEx(Class_Opt, sec, "Theta_C").param_value,
		   ["Theta_P"] = getParamEx(Class_Opt, sec, "Theta_P").param_value,
		   ["offerqty"] = getParamEx(Class_Opt, sec, "offerdepth").param_value
		   }
end

function find_zero(f, x_left, x_right, eps, par2)
   eps = eps or 0.0000000001   -- precision
   local f_left, f_right = f(x_left, par2), f(x_right, par2)
   assert(x_left <= x_right and f_left * f_right <= 0, "Wrong diapazone")
   while x_right - x_left > eps do
      local x_middle = (x_left + x_right) / 2
      local f_middle = f(x_middle, par2)
      if f_middle * f_left > 0 then
         x_left, f_left = x_middle, f_middle
      else
         x_right, f_right = x_middle, f_middle
      end
   end
   return (x_left + x_right) / 2
end

function find_volatility_by_price(f, x_left, x_right, eps, P, D, S, Optiontype, price, sec)
   local count=0;
   if price<=0 then
   	return nil
   end
   if Optiontype=="Call" then
	if price<=math.max(0, P-S) then
		return nil
	end
	if price>P then
   		return nil
	end
   else
	if price<=math.max(0, S-P) then
		return nil
	end
	if price>S then
   		return nil
	end
   end
   eps = eps or 0.0000000001   -- precision
   local f_left, f_right = f(x_left, P, D, S, Optiontype)-price, f(x_right, P, D, S, Optiontype)-price
   --assert(x_left <= x_right and f_left * f_right <= 0, "Wrong diapazone "..sec.." "..x_left.." "..x_right.." "..f_left+price.." "..f_right+price)
   assert(x_left <= x_right and f_left <= f_right, "Wrong diapazone "..sec.." "..x_left.." "..x_right.." "..f_left+price.." "..f_right+price)
   while f_right - f_left > eps do
      local x_middle = (x_left + x_right) / 2
      local f_middle = f(x_middle, P, D, S, Optiontype)-price
	  --message(""..f_left.." "..f_middle.." "..f_right);
      if f_middle * f_left > 0 then
         x_left, f_left = x_middle, f_middle
      else
         x_right, f_right = x_middle, f_middle
      end
	  if count>1000 then
	  	message("count > 1000")
	  	break
	  end
	  count=count+1
   end
   return (x_left + x_right) / 2
end

function my_func(x,par2)
	count=count+1
	par2 = par2 or 0
   return 200/(x+x^2+x^3+x^4+x^5) - 0.00001001+par2-20
end

function FindVolatility(sec, price, settleprice) -- �� ���� ���� ������ ������������� �� ����� ����
   local localfut = getParamEx(Class_Opt, sec, "optionbase").param_image
   local strike = tonumber(getParamEx(Class_Opt, sec, "strike").param_value)
   Optiontype=getParamEx(Class_Opt, sec, "optiontype").param_image

   local S = strike -- tonumber(string.sub(sec, 3, 7)) -- ������ �������
   --local V = tonumber(getParamEx(Class_Opt, sec, "volatility").param_value) / 100 -- ������������� ������� � �����
   local D = tonumber(getParamEx(Class_Opt, sec, "days_to_mat_date").param_value) / Year -- ����� ���� �� ��������� � ����� ����

   local P
   if settleprice~=nil then
   	P = settleprice -- ������� ���� ��������
   else
   	P = tonumber(getParamEx(Class_Fut, localfut, "settleprice").param_value) -- ������� ���� ��������
   end
   return find_volatility_by_price(GreekTheorPrice, 0.00000001, 1, 0.5, P, D, S, Optiontype, price, sec)
end

function Options3(sec, volatility, settleprice) -- ������� ������� ��������� �������
   local localfut = getParamEx(Class_Opt, sec, "optionbase").param_image
   local strike = getParamEx(Class_Opt, sec, "strike").param_value
   Optiontype=getParamEx(Class_Opt, sec, "optiontype").param_image
   --if Optiontype~="Put" then
   --	iscall = true
   --else
   --	iscall = false
   --end

   local S = strike -- tonumber(string.sub(sec, 3, 7)) -- ������ �������
   local V;
   if volatility~=nil then
   	V = volatility
   else
   	V = tonumber(getParamEx(Class_Opt, sec, "volatility").param_value) / 100 -- ������������� ������� � �����
   end
   local D = tonumber(getParamEx(Class_Opt, sec, "days_to_mat_date").param_value) / Year -- ����� ���� �� ��������� � ����� ����

--   if IsSubscribed_Level_II_Quotes(Class_Fut, localfut)==false then
--      Subscribe_Level_II_Quotes(Class_Fut, localfut)
--   end
--   local qu = getQuoteLevel2 (Class_Fut, localfut)
--	if tonumber(qu.offer_count)>0 then
--		P=tonumber(qu.offer[1].price)
--		futofferprice=P;
--		futofferqty=qu.offer[1].quantity
--   		local d1 = (math.log(P / S) + V * V * D / 2) / (V * math.sqrt(D)) -- ����� �� ����������� ����������� ���������� ������!!!
----	message(""..V.." "..d1.." "..P.." "..S.." "..D)
--		local d2 = d1 - V * math.sqrt(D)
--		local ExchangCall, ExchangPut = Black_Scholes(P, S, V, D)
--		if iscall then
--			-- ������ call ������� ������ ������� ��� ��������� ��������, ������ �������� �� ���� �������
--			ExchangSell = ExchangCall
--   			STheta, STheta_C, STheta_P, SDelta_C, SProfit_C, SDelta_P, SProfit_P = GreekCalc2(d1, d2, P, V, D, S)
--   			message(sec.." "..STheta.." "..STheta_C.." "..STheta_P)
--		else
--			ExchangBay = ExchangPut
--			BTheta, BTheta_C, BTheta_P, BDelta_C, BProfit_C, BDelta_P, BProfit_P = GreekCalc2(d1, d2, P, V, D, S)
--   			message(sec.." "..BTheta.." "..BTheta_C.." "..BTheta_P)
--		end
--	else
--		if datemesquotesoffer~=os.date() then
--			message("�� �����, � ������� �������� - ����� (offer)"..localfut)
--			datemesquotesoffer=os.date()
--		end
--		ExchangSell=100000
--		ExchangBay=0
--		ExchangBay=GeneriruemOshibku
--	end
--	--message("ExchangBay1"..ExchangBay)
--	if tonumber(qu.bid_count)>0 then
--		-- !!! ����� ��� �������, ��� �� ���� ������� �����-�� �������� ��������... if (Operation=="�������" and iscall)or(Operation=="�������" and not iscall) then
--		P=tonumber(qu.bid[tonumber(qu.bid_count)].price)
--		futbidprice=P
--		futbidqty=qu.bid[tonumber(qu.bid_count)].quantity
--   		local d1 = (math.log(P / S) + V * V * D / 2) / (V * math.sqrt(D))
--   		local d2 = d1 - V * math.sqrt(D)
--		local ExchangCall, ExchangPut = Black_Scholes(P, S, V, D)
--		if iscall then
--			-- ������ call ������� ������ ������� ��� ��������� �������, ������ ������� �� ���� �������
--			ExchangBay = ExchangCall
--			BTheta, BTheta_C, BTheta_P, BDelta_C, BProfit_C, BDelta_P, BProfit_P = GreekCalc2(d1, d2, P, V, D, S)
--   			message(sec.." "..BTheta.." "..BTheta_C.." "..BTheta_P)
--		else
--			ExchangSell = ExchangPut
--			STheta, STheta_C, STheta_P, SDelta_C, SProfit_C, SDelta_P, SProfit_P = GreekCalc2(d1, d2, P, V, D, S)
--   			message(sec.." "..STheta.." "..STheta_C.." "..STheta_P)
--		end
--		--qu.bid[1].quantity
--	else
--		if datemesquotesbid~=os.date() then
--			message("�� �����, � ������� �������� - ����� (bid)"..localfut)
--			datemesquotesbid=os.date()
--		end
--		ExchangSell=100000
--		ExchangBay=0
--		ExchangBay=GeneriruemOshibku
--	end
	--message("ExchangBay2"..ExchangBay)
   local P
   if settleprice~=nil then
   	P = settleprice
   else
   	P = tonumber(getParamEx(Class_Fut, localfut, "settleprice").param_value) -- ������� ���� ��������
   end
   local Delta, Gamma, Theta, Vega, Rho, TheorPrice = Greek(V, P, D, S, Optiontype)
   --local d1 = (math.log(P / S) + V * V * D / 2) / (V * math.sqrt(D))
   --local d2 = d1 - V * math.sqrt(D)
   --local Exchang = tonumber(getParamEx(Class_Opt, sec, "theorprice").param_value) -- ������������� ���� �����
   --local Exchang2, Exchang3 = Black_Scholes(P, S, V, D)
	
   --if iscall then
   --	Exchang3=Exchang2
   --end
   --local Theta = (-P * V * pN(d1)) / (2 * math.sqrt(D)) -- (-P * V * math.exp(-D) * pN(d1)) / (2 * math.sqrt(D))
   --local Theta_C = round((Theta - (S * math.exp(-D) * N(d2)) + P * math.exp(-D) * N(d1)) / Year, 0) -- ����� Call
   --local Theta_P = round((Theta + (S * math.exp(-D) * N(-d2)) - P * math.exp(-D) * N(-d1)) / Year, 0) -- ����� Put
   --local Delta_C = round(math.exp(-D) * N(d1), 2) -- ������ Call
   --local Profit_C = round(Step_Fut * Delta_C, 0) -- ������ ��� Call
   --local Delta_P = -round(-math.exp(-D) * N(-d1), 2) -- ������ Put
   --local Profit_P = round(Step_Fut * Delta_P, 0) -- ������ ��� Put
	--message(Theta.." "..Theta_C.." "..Theta_P)
   return {["TheorPrice"] = round(TheorPrice,0),
		   ["OptCode"] = sec,
		   ["settleprice"] = P,
		   ["futers"] = localfut,
		   ["Optiontype"] = Optiontype,
		   ["days_to_mat_date"] = D*Year,
		   ["Theta"] = Theta,
   		   ["Gamma"]= Gamma
   		--   ,
   		--   ["ExchangSell"]= ExchangSell,
           --["BDelta_C"] = BDelta_C,
           --["SDelta_C"] = SDelta_C,
           --["BDelta_P"] = BDelta_P,
           --["SDelta_P"] = SDelta_P,
		   --["SettlePrice"] = P,
		   --["iscall"] = iscall,
		   --["bid"] = tonumber(getParamEx(Class_Opt, sec, "bid").param_value),
		   --["futbidprice"] = futbidprice,
		   --["futbidqty"] = futbidqty,
		   --["offer"] = tonumber(getParamEx(Class_Opt, sec, "offer").param_value),
		   --["futofferprice"] = futofferprice,
		   --["volatility"] = V,
		   --["bidqty"] = getParamEx(Class_Opt, sec, "biddepth").param_value,
		   --["Theta_C"] = getParamEx(Class_Opt, sec, "Theta_C").param_value,
		   --["Theta_P"] = getParamEx(Class_Opt, sec, "Theta_P").param_value,
		   --["offerqty"] = getParamEx(Class_Opt, sec, "offerdepth").param_value
		   }
end

local math_abs, math_sqrt, math_exp, math_max, math_log = math.abs, math.sqrt, math.exp, math.max, math.log

function Black_Scholes(F, S, V, T)
-- F: ������� ���� �������� 
-- S: ������
-- V: �������������
-- T: ����� � ����� ���� �� ��������� ����� �������� ������� (4 / 365)
    if T <= 0 then
        local Call = math_max(F - S, 0)
        local Put = Call + S - F
        return Call, Put -- ������������� ����
    end
    local d1 = (math_log(F / S) + V * V * T / 2) / (V * math_sqrt(T))
    local d2 = d1 - V * math_sqrt(T)
   
  local Call = round(F * N(d1) - S * N(d2),0)
  local Put = Call + S - F
  return Call, Put
end

function N(x) -- ������� ����������� ��������
   if x > 10 then return 1
   elseif x < -10 then return 0
   else
      local t = 1 / (1 + 0.2316419 * math.abs(x))
      local p = 0.3989423 * math.exp(-0.5 * x * x) * t * ((((1.330274 * t - 1.821256) * t + 1.781478) * t - 0.3565638) * t + 0.3193815)
      if x > 0 then p = 1 - p end
      return p
   end
end
function pN(x) -- �������, ����������� �� ����������� ��������
   return math.exp(-0.5 * x * x) / math.sqrt(2 * math.pi)
end
function round(num, idp) -- ������� ��������� �� ���������� ���������� ������
   local mult = 10^idp
   return math.floor(num * mult + 0.5) / mult
end

function GreekCalc2(d1, d2, P, V, D, S)
	--message(""..V.." "..d1.." "..d1.." "..d1.." "..d1)
   local Theta = (-P * V * math.exp(-D) * pN(d1)) / (2 * math.sqrt(D))
   local Theta_C = round((Theta - (S * math.exp(-D) * N(d2)) + P * math.exp(-D) * N(d1)) / Year, 0) -- ����� Call
   local Theta_P = round((Theta + (S * math.exp(-D) * N(-d2)) - P * math.exp(-D) * N(-d1)) / Year, 0) -- ����� Put
   local Delta_C = round(N(d1), 2) -- ������ ��� ��������� ������ �������?... round(math.exp(-D) * N(d1), 2) -- ������ Call
   local Profit_C = round(Step_Fut * Delta_C, 0) -- ������ ��� Call
   local Delta_P = round(-N(-d1), 2) -- ������ ��� ��������� ������ �������?... -round(-math.exp(-D) * N(-d1), 2) -- ������ Put
   local Profit_P = round(Step_Fut * Delta_P, 0) -- ������ ��� Put
   return Theta, Theta_C, Theta_P, Delta_C, Profit_C, Delta_P, Profit_P
end

function Greek(b, S, Tt, K, Optiontype)
   --local b = tmpParam.volatility / 100 --"b" ������������� ���������� (���������� ������ �� ���������) �������� �����.
   --local S = tmpParam.settleprice --"S" ������� ���� �������� �����;
   --local Tt = D / YearLen --"T-t" ����� �� ��������� ����� ������� (������ �������);
   --local K =  tmpParam.strike --"K" ���� ���������� �������;
   local r = RiskFree --"r" ����������� ���������� ������;
   local d1 = (math.log(S / K) + (r + b * b * 0.5) * Tt) / (b * math.sqrt(Tt))
   local d2 = d1-(b * math.sqrt(Tt))
   
local Delta = 0
local Gamma = 0
local Theta = 0
local Vega = 0
local Rho = 0
local TheorPrice = 0

local e = math.exp(-1 * r * Tt)
   
   Gamma = pN(d1) / (S * b * math.sqrt(Tt))
   Vega = S * e * pN(d1) * math.sqrt(Tt)
   
   Theta = (-1 * S * b * e * pN(d1)) / (2 * math.sqrt(Tt))

   
   if Optiontype == "Call" then
      Delta = e * N(d1)
      Theta = Theta - (r * K * e * N(d2)) + r * S * e * N(d1)
      ----Theta = Theta - (r * K * e * N(d2))
      Rho = K * Tt * e * N(d2)
	  TheorPrice = S*e*N(d1)-e*K*N(d2) -- ��� �������� �, ����� ��������������� , ��� q ����� ����� �� ������ https://en.wikipedia.org/wiki/Greeks_%28finance%29
	  --message(""..e);
   else
      Delta = -1 * e * N(-1*d1)
      Theta = Theta + (r * K * e * N(-1 * d2)) - r * S * e * N(-1 * d1)
      ----Theta = Theta + (r * K * e * N(-1 * d2))
      Rho = -1 * K * Tt * e * N(-1 * d2)
	  TheorPrice = e*K*N(-d2)-S*e*N(-d1) -- ��� �������� �, ����� ��������������� , ��� q ����� ����� �� ������ https://en.wikipedia.org/wiki/Greeks_%28finance%29
   end
   

   return Delta, 100 * Gamma,
   Theta / Year,
   Vega / 100,
   Rho / 100,
   TheorPrice
   
end

function GreekTheorPrice(b, S, Tt, K, Optiontype)
   --local b = tmpParam.volatility / 100 --"b" ������������� ���������� (���������� ������ �� ���������) �������� �����.
   --local S = tmpParam.settleprice --"S" ������� ���� �������� �����;
   --local Tt = D / YearLen --"T-t" ����� �� ��������� ����� ������� (������ �������);
   --local K =  tmpParam.strike --"K" ���� ���������� �������;
   local r = RiskFree --"r" ����������� ���������� ������;
   local d1 = (math.log(S / K) + (r + b * b * 0.5) * Tt) / (b * math.sqrt(Tt))
   local d2 = d1-(b * math.sqrt(Tt))
   
local TheorPrice = 0

local e = math.exp(-1 * r * Tt)
   
   if Optiontype == "Call" then
	  TheorPrice = S*e*N(d1)-e*K*N(d2) -- ��� �������� �, ����� ��������������� , ��� q ����� ����� �� ������ https://en.wikipedia.org/wiki/Greeks_%28finance%29
   else
	  TheorPrice = e*K*N(-d2)-S*e*N(-d1) -- ��� �������� �, ����� ��������������� , ��� q ����� ����� �� ������ https://en.wikipedia.org/wiki/Greeks_%28finance%29
   end

   return TheorPrice
   
end
